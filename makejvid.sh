#!/bin/bash

# This is a simple ffmpeg script that creates highly-compressed journal videos "jvids" from webcam recording and uploads them to a remote location. These videos are meant to emphasize audio over quality of  video - JTR

# create a filename based on today's date (yyyy-mm-dd format)
filename=$(date +%F).mp4

# source additional variables to use for scp below 
. $HOME/Personal/scriptsettings 

# a simple scp line to send files back to server when done
function scp_remote {
    scp $vid_in_path$filename go:$jvid_out_path
}

# ask the user if to upload to remote.
# if yes,, invoke the scp function above.
# if no, exit
# if invalid answer, run this function again

function upload_remote {
    read -r -p "Upload to remote server? y/n" ans
    case $ans in
	y)
	    scp_remote
	    ;;
	n)
	    exit 1
	    ;;
	*)
	    echo "not a valid answer."
	    upload_remote 
    esac
}


# add all .webm files into a vids.txt, to be used to for contact below
for file in ~/Videos/Webcam/*.webm; do
    echo "file '"$file"'" >> $vid_in_path/vids.txt;
done

# Use ffmpeg to concat and compress the video with this options:
# 640 px wide, high compression, incrase volume 100%
# delete the list vids.txt after completion

ffmpeg -f concat -safe 0 -i $vid_in_path/vids.txt -vf scale=640:-1 -af volume=2.0 -crf 30 $vid_in_path$filename && rm $vid_in_path/vids.txt

# ask if to upload to remote server
upload_remote

